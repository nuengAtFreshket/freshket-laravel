<!doctype html>
<html lang="{{ app()->getLocale() }}">
    @include('layouts.head')
    <body>
        @yield('content')
    </body>
</html>
