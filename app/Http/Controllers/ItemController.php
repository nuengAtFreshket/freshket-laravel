<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Item;

class ItemController extends Controller
{
    public function index()
    {
    	return view('item.index');
    }

    public function show(Item $item_id)
    {
    	$item=Item::find($item_id);
    	return $item;
    }
}
